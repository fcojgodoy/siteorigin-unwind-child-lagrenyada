<?php

/**
 * Enqueue theme scripts and styles.
 */
function siteorigin_unwind_child_scripts() {

	// Child theme stylesheet.
	wp_enqueue_style( 'unwind-child', get_stylesheet_directory_uri() . '/style.css' );

}
add_action( 'wp_enqueue_scripts', 'siteorigin_unwind_child_scripts', 8 );


// Woocommerce function require files
require get_stylesheet_directory() . '/inc/woocommerce/single-product-tabs.php';


// Carbon Fields
use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_attach_term_meta' );
function crb_attach_term_meta() {
    Container::make( 'term_meta', __( 'Woocommerce Term Options', 'siteorigin-unwind' ) )
        ->where( 'term_taxonomy', '=', 'pa_farines' )
        ->or_where( 'term_taxonomy', '=', 'pa_ingredients' )
        ->add_fields( array(
            Field::make( 'checkbox', 'crb_organic_ingred', __( 'Organic farming ingredient', 'siteorigin-unwind' ) )
                ->set_option_value( 'yes' )
                ->set_help_text( __( 'If this is marked, this ingredient will appear as organic in all the products that carry it.', 'siteorigin-unwind' ) ),
        ) );
}

add_action( 'after_setup_theme', 'crb_load' );
function crb_load() {
    require_once( 'vendor/autoload.php' );
    \Carbon_Fields\Carbon_Fields::boot();
}
